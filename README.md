# Horizontal Timeline Scroller
___

## Overview

The timeline scroller is a jQuery plugin that creates an interactive, chronological timeline.  It takes events that you've added to the HTML and generates a functional UI for them placed appropriately along the timeline.

## Dependencies

1. jQuery 1.11+
2. jQuery Mousewheel Plugin 3.1.12+

## Features

### Eras
Define different periods/eras/ages.

### Events
Build out your events in HTML as opposed to lengthy SEO-unfriendly JSON definitions.
Events pop up when rolled over.

### Custom Units
Define a custom range of units and labels on your timeline.

## Usage

```html
	<div id="timeline">
		<ul class="plot">
			<li class="lrg" date="5000BC">
				<div>
					<h1>In The Beginning</h1>
				</div>
			</li>
			<li date="4850BC">
				<div>
					<h1>Some stuff</h1>
				</div>
			</li>
			<li date="4600BC">
				<div>
					<h1>More stuff</h1>
				</div>
			</li>
		</ul>
	</div>
```

```javascript
	$('#timeline').Timeline({
		eras: [{
			label: 'Before Christ',
			start: 5000,
			end: 1,
			step: 5,
			abbr: 'BC',
			yearInterval: 100
		}, {
			label: 'Anno Domini',
			start: 0,
			end: 2015,
			step: 5,
			abbr: 'AD',
			yearInterval: 100
		}]
	});
```

## To Do

1. Add support for crossfading page backgrounds
2. Third level of event "size" (currently only small and large circles)