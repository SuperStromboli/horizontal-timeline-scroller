(function ($) {

    $.fn.Timeline = function (options) {
		var self = this;		
		
		// access to jQuery and DOM versions of element
		self.el = self[0];
		
		// add a reverse reference to the DOM object
        self.data("Timeline", self);
		
		// sets all variables needed to build the timeline
		self.init = function(){
		    // set properties
		    self.config = options.config;
			self.eras = options.eras;
			
			// set containers
			self.$plot = self.children('.plot');
			self.$units = $('<ul class="units"></ul>').insertAfter(self.$plot);
			self.$eras = $('<ul class="eras"></ul>').insertAfter(self.$units);
		
			// horiz scroll wheel
			$(window).on('mousewheel', function(e, d){
				var $b = $('body'),
					left = $b.scrollLeft();
				$b.scrollLeft(left - (d * 30));
				
				// adjust position of era label
				self.setCurrentEra();
			});
			
			// horiz scroll
			$(window).on('scroll', function(e){        
				// adjust position of era label
				self.setCurrentEra();
			});
			
			// positioning
			/*$(window).on('resize', function(){
				// vertically center timeline
				self.css({
					top: $(window).height() / 2
				});
			}).trigger('resize');*/
			
			// build it
			self.build();
		}
		
		// physically builds the timeline
		self.build = function(){
			
		    var curLeft = 30,
                stepWidth = 10;

			self.eras.forEach(function(e, idx){
			    var curYear = e.start,
					totalYears = Math.abs(e.end - e.start),
					step = e.end < e.start ? -e.step : e.step,
					go = true,
					startLeft = curLeft;

			    // draw eras
			    var $bar = $('<li><span>' + e.label + '</span></li>'),
			        eraLeft = idx == 0 ? 0 : startLeft;
			    $bar.appendTo(self.$eras);
				
			    do {
			        // add unit
			        var $li = $('<li></li>'),
						yearInterval = e.yearInterval;
					
			        if (curYear % yearInterval == 0)
			            $li.addClass('whole').html('<span class="year">' + curYear + '</span><br><span class="label">' + e.abbr + '</span>');
					
			        $li.css({ left: curLeft }).appendTo(self.$units);
					
			        // position any events for this date
			        var $dates = self.$plot.children('[data-date=' + curYear + e.abbr + ']');
			        if ($dates.length > 0)
			            $dates.each(function(idx2, li){
			                $(li).css({ left: 18 + curLeft - ($(li).width() / 2) });
			            });
					
                    // add to X pos and update the current year
			        curLeft += stepWidth;
			        curYear += step;
			        console.log({
                        startX: startLeft,
			            X: curLeft,
                        Year: curYear
			        });
					
			        // check to see if we're at the end of the era
			        go = e.end < e.start ? curYear > e.end : curYear < e.end;
			    } while (go);

			    // set width of era
			    $bar.width(totalYears * stepWidth);
			});    
			
			// adjust positions of event containers
			self.$plot.children().each(function(idx, li){
				var $div = $(li).children('div'),
					height = $div.height(),
					leftOffset = -($div.width() / 2) + ($(li).width() / 2);
				
				// keep from falling off the screen
				if ($div.offset().left + leftOffset < 0)
					leftOffset = 0;
				
				// set position
				$div.css({
					top: ($(li).position().top - height - 10),
					marginLeft: leftOffset
				});
			});
			
			// set width of timeline to match units
			self.width(curLeft);
		}
		
		// updates the current era by checking the viewport with every scroll change and sets its position
		self.setCurrentEra = function(){
			
			var self = this,
				scrollLeft = $('body').scrollLeft();
			
			// reset era
			self.$curEra = null;
			
			$('.eras li').each(function(idx, e){
				var leftBoundary = parseInt($(e).css('left')),
					rightBoundary = leftBoundary + $(e).width();
				
				if (scrollLeft > leftBoundary && scrollLeft < rightBoundary) {
					self.$curEra = $(e);
					var $label = self.$curEra.children('span');
					
					// position the era label, if needed
					$label.css({
                        left: scrollLeft - self.$curEra.offset().left
					});
					
					// reset all inactive label positions
					self.$curEra.siblings().children('span').css({ left: 0 });
					
					return false;
				}
			});
			
			// also reset if no era was found
			if (self.$curEra == null)
				$('.eras li').children('span').css({ left: 0 });
		}
		
		// start by calling init
		self.init();
	}
	
})(jQuery);